<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Student>
 */
class StudentModelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'nim' => fake()->randomNumber(),
            'gender' => fake()->randomElement(['L', 'P']),
            'place_birth' => fake()->city(),
            'date_birth' => fake()->date(),
            'email' => fake()->email(),
            'phone' => fake()->randomNumber(),
            'address' => fake()->address(),
            'photo' => ""
        ];
    }
}

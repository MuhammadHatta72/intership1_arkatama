<x-layouts.app>
    <x-slot:title>Students</x-slot:title>
    <livewire:pages.admin.students.create-student />
    <livewire:pages.admin.students.update-student />
    <livewire:pages.admin.students.student-modal />


    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title">
                <div class="d-flex align-items-center position-relative my-1 gap-3">
                    <div class="d-flex align-items-center position-relative my-1 mx-5">
                        <x-atoms.select class="form-select form-select-solid w-200px" data-action-filter
                            data-table-id="mahasiswas-table" data-filter="0">
                            <option value="">Pilih Filter</option>
                            <option value="3">NIM</option>
                            <option value="2">Nama Lengkap</option>
                            <option value="4">Jenis Kelamin</option>
                            <option value="5">Tempat Lahir</option>
                        </x-atoms.select>
                    </div>
                    <div class="d-flex align-items-center position-relative my-1">
                        <span class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></span>
                        <input type="text" data-table-id="mahasiswas-table"
                            class="form-control form-control-solid w-300px ps-13" placeholder="Cari Mahasiswa"
                            id="searchStudent" />
                    </div>    
                </div>
            </div>

            <div class="card-toolbar">
                <!--begin::Toolbar-->
                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                    <!--begin::Add user-->
                    @can('admin-students-create')
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#add-student_modal">
                            <i class="ki-duotone ki-plus fs-2"></i>
                            <span>Add Students</span>
                        </button>
                    @endcan
                </div>
            </div>
        </div>

        <div class="card-body py-4">
            <div class="table-responsive">
                {{ $dataTable->table() }}
            </div>
        </div>
    </div>
    
    @push('css')
        <link rel="stylesheet" href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}">
    @endpush

    @push('scripts')
        <script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>

        {{ $dataTable->scripts() }}

        {{-- <script>
            $('#add-mahasiswa_modal, #edit-mahasiswa_modal').on('hidden.bs.modal', function(e) {
                $(this).find('form').trigger('reset');
            });
        </script> --}}
        <script>
            $(document).ready(function () {
            const table = window.LaravelDataTables['students-table'];
            const search = document.getElementById('searchStudent');
            const filter = document.querySelector('select[data-action-filter]');
            $('#searchStudent').on('keyup', function() {
                    var column = $('select[data-action-filter]').val();
                    var value = this.value;

                    if (column === "") {
                        table.search(value).draw();
                    } else {
                        table.column(column).search(value).draw();
                    }
                });

                $('select[data-action-filter]').on('change', function() {
                    search.placeholder = `Cari berdasarkan ${filter.options[filter.selectedIndex].text}`;
                    table.search('').columns().search('').draw();
                    $('#searchStudent').val('');
                });

        });


        </script>
    @endpush

</x-layouts.app>

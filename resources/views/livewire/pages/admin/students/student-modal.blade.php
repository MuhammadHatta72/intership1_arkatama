<div>
  
</div>

@push('scripts')
  <script>
    document.addEventListener('livewire:initialized', () => {
      function refreshTable() {
        window.LaravelDataTables['students-table'].ajax.reload();
      };
      @this.on('student-deleted', () => {
        refreshTable();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Mahasiswa berhasil dihapus',
          showConfirmButton: true,
          willClose: () => {
            location.reload();
          }
        });
      });

    });
  </script>
@endpush

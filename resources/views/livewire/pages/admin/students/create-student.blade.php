<div>
    {{-- Add --}}
  <x-mollecules.modal id="add-student_modal" action="store" wire:ignore.self>
    <x-slot:title>Add Student</x-slot:title>
    <div class="">
      <div class="mb-6">
        <x-atoms.form-label required>Nama Lengkap</x-atoms.form-label>
        <x-atoms.input name="name" wire:model='form.name' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>NIM</x-atoms.form-label>
        <x-atoms.input name="nim" wire:model='form.nim' type="number" />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Jenis Kelamin</x-atoms.form-label>
        <select wire:model="form.gender" class="form-select " data-control="select2" data-hide-search="true"
          data-placeholder="Pilih Jenis Kelamin" name="gender">
          <option value="">Pilih Jenis Kelamin</option>
          <option value="L">Laki-laki</option>
          <option value="P">Perempuan</option>
        </select>
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Tempat Lahir</x-atoms.form-label>
        <x-atoms.input name="place_birth" wire:model='form.place_birth' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Tanggal Lahir</x-atoms.form-label>
        <x-atoms.input name="date_birth" type="date" wire:model='form.date_birth' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Email</x-atoms.form-label>
        <x-atoms.input name="email" type="email" wire:model='form.email' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Nomor Telepon</x-atoms.form-label>
        <x-atoms.input name="phone" wire:model='form.phone' type="number" />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Alamat</x-atoms.form-label>
        <x-atoms.textarea name="address" wire:model='form.address' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Foto Profil</x-atoms.form-label>
        <x-atoms.input name="photo" type="file" wire:model='form.photo' accept="image/*"/>
      </div>
      <x-slot:footer>
        <button class="btn-primary btn" type="submit">Submit</button>
      </x-slot:footer>
    </div>
  </x-mollecules.modal>
</div>

@push('scripts')
  <script>
    document.addEventListener('livewire:initialized', () => {
      function refreshTable() {
        window.LaravelDataTables['students-table'].ajax.reload();
      };
      @this.on('student-added', () => {
        $('#add-student_modal').modal('hide');
        refreshTable();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Mahasiswa berhasil ditambahkan',
          showConfirmButton: true,
          willClose: () => {
            location.reload();
          }
        });
      });
    });
  </script>
@endpush

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\StudentsDataTable;


class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:admin-students');
    }

    public function index(StudentsDataTable $datatable){
        return $datatable->render('pages.admin.students.index');
    }
}

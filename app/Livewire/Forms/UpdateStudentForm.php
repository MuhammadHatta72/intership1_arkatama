<?php

namespace App\Livewire\Forms;

use Livewire\Attributes\Validate;
use Livewire\Form;
use Livewire\WithFileUploads;

class UpdateStudentForm extends Form
{
    use WithFileUploads;

    public $id;
    // #[Validate('required|max:255')]
    public $name = "";
    // #[Validate('required|max:10|unique:students,nim')]
    public $nim = "";
    // #[Validate('required|max:1')]
    public $gender = "";
    // #[Validate('required|max:100')]
    public $place_birth = "";
    // #[Validate('required|date')]
    public $date_birth = "";
    // #[Validate('required','email','max:255','unique:students,email')]
    public $email = "";
    // #[Validate('required|max:13')]
    public $phone = "";
    // #[Validate('required|max:255')]
    public $address = "";
    // #[Validate('nullable|image|max:2048')]
    public $photo = "";
    // #[Validate('nullable|image|max:2048')]
    public $photo_new = "";
}

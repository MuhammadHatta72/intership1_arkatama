<?php

namespace App\Livewire\Forms;

use Livewire\Attributes\Validate;
use Livewire\Form;

class StudentForm extends Form
{
    #[Validate('required|max:255')]
    public $name = "";
    #[Validate('required|max:10|unique:students,nim')]
    public $nim = "";
    #[Validate('required|max:1')]
    public $gender = "";
    #[Validate('required|max:100')]
    public $place_birth = "";
    #[Validate('required|date')]
    public $date_birth = "";
    #[Validate('required|email|max:100|unique:students,email')]
    public $email = "";
    #[Validate('required|max:13')]
    public $phone = "";
    #[Validate('required|max:255')]
    public $address = "";
    #[Validate('nullable|image|max:2048')]
    public $photo = "";
}

<?php

namespace App\Livewire\Pages\Admin\Students;

use Livewire\Component;

class StudentsTableAction extends Component
{
    public $students;
    public function mount($students){
        $this->students = $students;
    }
    
    public function render()
    {
        return view('livewire.pages.admin.students.students-table-action');
    }
}

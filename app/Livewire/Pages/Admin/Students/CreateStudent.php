<?php

namespace App\Livewire\Pages\Admin\Students;

use Livewire\Component;
use App\Models\IconsModel;
use Livewire\WithFileUploads;
use App\Models\StudentModel;
use App\Models\RolesModel;
use Livewire\Attributes\On;
use Livewire\Attributes\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule as ValidationRule;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Livewire\Forms\StudentForm;

class CreateStudent extends Component
{

    use WithFileUploads;

    public StudentForm $form;


    public $id;

    public function store(){
        $this->validate();

        DB::beginTransaction();
        try {
            $filename = "";
            $student = new StudentModel();
            $student->name = $this->form->name;
            $student->nim = $this->form->nim;
            $student->gender = $this->form->gender;
            $student->place_birth = $this->form->place_birth;
            $student->date_birth = $this->form->date_birth;
            $student->email = $this->form->email;
            $student->phone = $this->form->phone;
            $student->address = $this->form->address;
            if($this->form->photo !== ""){
                $filename = 'students/' . $this->form->nim . '_profil.' . $this->form->photo->extension();
                $this->form->photo->storeAs('public/', $filename);   
                $student->photo = $filename;
            }else{
                $student->photo = "";
            }
            $student->save();
            DB::commit();
            $this->reset();
            $this->dispatch("student-added");
        } catch (\Throwable $th) {
            $this->reset();
            $this->dispatch("student-error");
            DB::rollback();
        }
    }

    public function render()
    {
        return view('livewire.pages.admin.students.create-student');
    }
}

<?php

namespace App\Livewire\Pages\Admin\Students;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\StudentModel;
use Livewire\Attributes\On;
use Livewire\Attributes\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule as ValidationRule;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class StudentModal extends Component
{

    use WithFileUploads;

    public $id;
    
    #[On('delete')] 
    public function delete($id){
        $student = StudentModel::find($id);
        if($student->photo !== ""){
            Storage::delete('public/' . $student->photo);
        }
        if($student->delete()){
            $this->dispatch("student-deleted");
        }
    }

    public function render()
    {
        return view('livewire.pages.admin.students.student-modal');
    }

    
}

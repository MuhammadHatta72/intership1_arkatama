<?php

namespace App\Livewire\Pages\Admin\Students;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\StudentModel;
use Livewire\Attributes\On;
use Livewire\Attributes\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule as ValidationRule;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Livewire\Forms\UpdateStudentForm;

class UpdateStudent extends Component
{
    use WithFileUploads;

    public UpdateStudentForm $form;

    #[On('edit')]
    public function edit($id){
        $student = StudentModel::find($id);
        $this->form->name = $student->name;
        $this->form->nim = $student->nim;
        $this->form->gender = $student->gender;
        $this->form->place_birth = $student->place_birth;
        $this->form->date_birth = $student->date_birth;
        $this->form->email = $student->email;
        $this->form->phone = $student->phone;
        $this->form->address = $student->address;
        $this->form->photo = $student->photo;
        $this->form->id = $student->id;
        $this->dispatch("student-edit");
    }

    public function update(){
        $this->form->validate([
            'name' => 'required|max:255',
            'nim' => [
                'required',
                'max:10',
                ValidationRule::unique('students', 'nim')->ignore($this->form->id),
            ],
            'gender' => 'required|max:1',
            'place_birth' => 'required|max:100',
            'date_birth' => 'required|date',
            'email' => [
                'required',
                'max:100',
                ValidationRule::unique('students', 'email')->ignore($this->form->id),
            ],
            'phone' => 'required|max:13',
            'address' => 'required|max:255',
            'photo_new' => 'nullable|image|max:2048',
        ]);
        $student = StudentModel::find($this->form->id);
        if($student){
            $student->name = $this->form->name;
            $student->nim = $this->form->nim;
            $student->gender = $this->form->gender;
            $student->place_birth = $this->form->place_birth;
            $student->date_birth = $this->form->date_birth;
            $student->email = $this->form->email;
            $student->phone = $this->form->phone;
            $student->address = $this->form->address;

            if($this->form->photo_new !== ""){
                if ($this->form->photo !== "") {
                    Storage::delete('public/' . $this->form->photo);
                }
                $filename = 'students/' . $this->form->nim . '_profil.' . $this->form->photo_new->extension();
                $this->form->photo_new->storeAs('public/', $filename);
                $student->photo = $filename;
            }
            $student->save();

            $this->reset();
            $this->dispatch("student-updated");
        }
    }

    public function render()
    {
        return view('livewire.pages.admin.students.update-student');
    }
}

<?php

namespace App\DataTables;

use App\Models\StudentModel;
use Livewire\Livewire;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class StudentsDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', 'students.action')
            ->addColumn('photo', 'students.photo')
            ->addIndexColumn()
            ->addColumn('action', function(StudentModel $val){
                return Livewire::mount('pages.admin.students.students-table-action', ['students' => $val]);
            })
            // tampilkan foto blank untuk testing
            ->editColumn('photo', function(StudentModel $val) {
                return $val->photo == "" ? "<img src='" . asset("assets/media/avatars/blank.png") . "' width='100px' height='100px' class='rounded-circle'>" : "<img src='" . asset("storage/" . $val->photo) . "' width='100px' height='100px' class='rounded-circle'>";
            })
            ->editColumn('gender', function(StudentModel $val) {
                return $val->gender == "L" ? "Laki-laki" : "Perempuan";
            })
            ->rawColumns(['photo', 'action'])
            ->setRowId('id');
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(StudentModel $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('students-table')
            ->columns($this->getColumns())
            ->minifiedAjax(script: "
                data._token = '" . csrf_token() . "';
                data._p = 'POST';
            ")
            // ->dom('lrtip')
            ->dom('rt' . "<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7'p>>",)
            ->addTableClass('table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer text-gray-600 fw-semibold')
            ->setTableHeadClass('text-start text-muted fw-bold fs-7 text-uppercase gs-0')
            ->drawCallbackWithLivewire(file_get_contents(public_path('assets/js/custom/table/_init.js')))
            ->orderBy(2)
            ->select(false)
            ->responsive(true)
            ->buttons([]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::computed('action')
                  ->title("No.")
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('photo')->title('Foto_Profil'),
            Column::make('name')->title('Nama Lengkap')->searchable(true),
            Column::make('nim')->title('NIM')->searchable(true),
            Column::make('gender')->title('Jenis Kelamin')->searchable(true),
            Column::make('place_birth')->title('Tempat Lahir')->searchable(true),
            Column::make('date_birth')->title('Tanggal Lahir')->searchable(true),
            Column::make('email')->searchable(true),
            Column::make('phone')->title('No. HP')->searchable(true),
            Column::make('address')->title('Alamat')->searchable(true),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Students_' . date('YmdHis');
    }
}
